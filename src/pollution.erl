%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. kwi 2017 21:22
%%%-------------------------------------------------------------------
-module(pollution).
-author("Karol").

%% API
-export([createMonitor/0, addStation/3, addValue/5, removeValue/4, getOneValue/4, getStationMean/3, getDailyMean/3, getAreaMean/4, testPoprawny1/0, testPoprawny2/0, testPoprawny3/0, testPoprawny4/0, testPoprawny5/0, testPoprawny6/0, getStationDailySumTest/0]).
-import(calendar, [local_time/0]).
-import(string, [lenght/1]).

-export_type([rodzaj/0]).

%patrz wyklad wprowadzenie do języka częśc 2, strona 22

-type rodzaj () :: 'PM10' | 'PM2,5' | 'Temperatura'.
          %-type wspolrzedne (X,Y) :: {X::float(), Y::float()}.
          %-type pomiar (Rodzaj, Czas) ::  { Rodzaj::rodzaj(), Czas::calendar:datetime()}.
          %-type stacja (Nazwa, Wspolrzedne) :: {Nazwa::nonempty_string(), Wspolrzedne::wspolrzedne()}.
          %-type monitor (Stacje, Pomiary) :: {Stacje::list(stacja), Pomiary::list(pomiar)}.

%-type wspolrzedne (X,Y) :: {X::float(), Y::float()}.

%-type wspolrzedne (X, Y) :: {X, Y}.

%-record(wspolrzedne, {x,y}).

-record(pomiar, {rodzaj, czas, wartosc}).
-record(stacja, {nazwa, wspolrzedne = {0,0}, pomiary = []}).
-record(monitor, {stacje = []}).

createMonitor() ->  #monitor {}.


addStation (Nazwa, Wspolrzedne, Monitor ) when is_list(Nazwa) , length(Nazwa) > 0, is_tuple(Wspolrzedne), is_record(Monitor, monitor) ->
  Nowa = #stacja {nazwa = Nazwa, wspolrzedne = Wspolrzedne},
  Stacje = Monitor#monitor.stacje,
  Monitor#monitor {stacje = [Nowa] ++ Stacje}.

addValue (Nazwa, Data, TypPomiaru, Wartosc, Monitor) when is_list(Nazwa), length(Nazwa) > 0->
  Nowy = #pomiar {rodzaj = TypPomiaru, czas = Data, wartosc = Wartosc},
  Stacje = Monitor#monitor.stacje,
  Stacja = lists:keyfind(Nazwa, 2, Stacje),
  Pomiary = Stacja#stacja.pomiary,
  ZaktualizowanaStacja = Stacja#stacja {pomiary = [Nowy] ++ Pomiary},
  ZaktualizowanaListaStacji = lists:keyreplace(Nazwa, 2, Stacje, ZaktualizowanaStacja),
  Monitor#monitor {stacje = ZaktualizowanaListaStacji};
%TODO: sprawdzanie redundancji i czy nie dodaje sie pomiaru do nieistniejacej stacji

addValue (Wspolrzedne, Data, TypPomiaru, Wartosc, Monitor) when is_tuple(Wspolrzedne) ->
  Stacje = Monitor#monitor.stacje,
  Stacja = lists:keyfind(Wspolrzedne, 3, Stacje),
  addValue(Stacja#stacja.nazwa, Data, TypPomiaru, Wartosc, Monitor).

% keymember(Key, N, TupleList) -> boolean()
% Returns true if there is a tuple in TupleList whose Nth element compares equal to Key, otherwise false.

%keyfind(Key, N, TupleList) -> Tuple | false
% Searches the list of tuples TupleList for a tuple whose Nth element compares equal to Key. Returns Tuple if such a tuple is found, otherwise false.
% Nazwa to drugi element, bo pierwszy jest atom identyikujący 'typ' rekordu

%keyreplace(Key, N, TupleList1, NewTuple) -> TupleList2
%Returns a copy of TupleList1 where the first occurrence of a T tuple whose Nth element compares equal to Key is replaced with NewTuple, if there is such a tuple T.


removeValue (Nazwa, Czas, TypPomiaru, Monitor) when is_list(Nazwa) ->
  Stacja = lists:keyfind(Nazwa, 2, Monitor#monitor.stacje),
  Pomiary = Stacja#stacja.pomiary,
  ZaktualizowanePomiary = usunPomiar(Pomiary, Czas, TypPomiaru),
  ZaktualizowanaStacja = Stacja#stacja {pomiary = ZaktualizowanePomiary},
  ZaktualizowanaListaStacji = lists:keyreplace(Nazwa, 2, Monitor#monitor.stacje, ZaktualizowanaStacja),
  Monitor#monitor {stacje = ZaktualizowanaListaStacji};


removeValue (Wspolrzedne, Czas, TypPomiaru, Monitor) when is_tuple(Wspolrzedne) ->
  Stacja = lists:keyfind(Wspolrzedne, 3, Monitor#monitor.stacje),
  Nazwa = Stacja#stacja.nazwa,
  removeValue(Nazwa, Czas, TypPomiaru, Monitor).

usunPomiar (Pomiary, Czas, TypPomiaru) ->
  [X || X<-Pomiary, ( ( element (1, element(3, X)) =/= element (1,Czas) ) or ( element (2, element(3, X)) =/= element (2,Czas) )  or ( element(2, X) =/= TypPomiaru ) ) ].
  %-record(pomiar, {rodzaj, czas, wartosc}).

getOneValue (Nazwa, Czas, TypPomiaru, Monitor) when is_list(Nazwa) ->
  Stacja = lists:keyfind(Nazwa, 2, Monitor#monitor.stacje),
  Pomiary = Stacja#stacja.pomiary,
  znajdzPomiar (Czas, TypPomiaru, Pomiary);


getOneValue (Wspolrzedne, Czas, TypPomiaru, Monitor) when is_tuple(Wspolrzedne) ->
  Stacja = lists:keyfind(Wspolrzedne, 3, Monitor#monitor.stacje),
  getOneValue(Stacja#stacja.nazwa, Czas, TypPomiaru, Monitor).

znajdzPomiar (_Czas, _TypPomiaru, []) -> -1;

znajdzPomiar (Czas, TypPomiaru, [H|T]) ->
  if element (1, H#pomiar.czas) == element (1, Czas),  element (2, H#pomiar.czas) == element (2, Czas), H#pomiar.rodzaj == TypPomiaru -> H#pomiar.wartosc;
     true -> znajdzPomiar(Czas, TypPomiaru, T)
  end.

rodzajPomiaru (Pomiar) -> element(2, Pomiar).


getStationMean (Nazwa, TypPomiaru, Monitor) when is_list(Nazwa) ->
  Stacja = lists:keyfind(Nazwa, 2, Monitor#monitor.stacje),
  %PrzefiltrowanePomiary = lists:filter(rodzajPomiaru, Stacja#stacja.pomiary) ,
  PrzefiltrowanePomiary = [X#pomiar.wartosc || X<-Stacja#stacja.pomiary, rodzajPomiaru(X) == TypPomiaru],
  LiczbaPomiarow = lists:flatlength(PrzefiltrowanePomiary),
  Suma = lists:foldl(fun (X, Y) -> X + Y end, 0, PrzefiltrowanePomiary),    %fun (X, Y) -> X + Y end to lambda. Mozna by tez uzyc samodzielnie zdefiniowanej funkcji suma (X, Y) -> X + Y.
  Suma / LiczbaPomiarow;                                                    %wowczas nalezaloby tam wstawić fun suma/2 a nie po prostu suma bo suma to atom, a potrzeba funkcji (WPP: error bad function)

getStationMean (Wspolrzedne, TypPomiaru, Monitor) when is_tuple(Wspolrzedne) ->
  Stacja = lists:keyfind(Wspolrzedne, 3, Monitor#monitor.stacje),
  getStationMean(Stacja#stacja.nazwa, TypPomiaru, Monitor).

getDailyMean (TypPomiaru, Data, Monitor)->
  LiczbaStacji = lists:flatlength(Monitor#monitor.stacje),
  case LiczbaStacji of
    0 -> -1.0;
    _ -> SrednieNaStacjach = [getStationDailySum(X, TypPomiaru, Data) || X<-Monitor#monitor.stacje], %SrednieNaStacjach ma postac listy następujących krotek: {LiczbaPomiarow na tej stacji, Suma ich wartosci}
      LiczbaPomiarow = lists:foldl(fun (X,Y) -> element (1, X) + Y end, 0, SrednieNaStacjach),
      SumaPomiarow = lists:foldl(fun (X,Y) -> element (2, X) + Y end, 0, SrednieNaStacjach),
      case LiczbaPomiarow of
        0 -> -1.0;
        _ -> SumaPomiarow/LiczbaPomiarow
      end

      %SumaSrednich = lists:foldl(fun (X, Y) -> element (2, getStationDailyMean(X, TypPomiaru, Data)) + Y end, 0, Monitor#monitor.stacje),
      %LiczbaPomiarow = lists:foldl(fun (X, Y) -> element (1, getStationDailyMean(X, TypPomiaru, Data)) + Y end, 0, Monitor#monitor.stacje),

  end.


getStationDailySum ({}, _TypPomiaru, _Data) -> {0,0};


getStationDailySum (Stacja, TypPomiaru, Data) ->
  PrzefiltrowanePomiary = [X#pomiar.wartosc || X<-Stacja#stacja.pomiary, rodzajPomiaru(X) == TypPomiaru, element (1, X#pomiar.czas) == Data],
  LiczbaPomiarow = lists:flatlength(PrzefiltrowanePomiary),
  case LiczbaPomiarow of
    0 -> {0,0};
    _ -> Suma = lists:foldl(fun (X, Y) -> X + Y end, 0, PrzefiltrowanePomiary),
      {LiczbaPomiarow, Suma}
  end.


getStationDailyMean(Stacja, TypPomiaru, Data) ->  %Stacja jako rekord! Nieuzywana, choc poprawna. Zamieniłem ją na getStationDailySum, bo wcześniejszy algorytm był niepoprawny
  PrzefiltrowanePomiary = [X#pomiar.wartosc || X<-Stacja#stacja.pomiary, rodzajPomiaru(X) == TypPomiaru, element (1, X#pomiar.czas) == Data],
  LiczbaPomiarow = lists:flatlength(PrzefiltrowanePomiary),
  Suma = lists:foldl(fun (X, Y) -> X + Y end, 0, PrzefiltrowanePomiary),
  case LiczbaPomiarow of
    0 -> 0;
    _ -> {LiczbaPomiarow, Suma / LiczbaPomiarow}
  end.


getAreaMean (Wspolrzedne, TypPomiaru, Promien, Monitor) when is_tuple(Wspolrzedne) ->
  PrzefiltrowaneStacje = [X || X<-Monitor#monitor.stacje, distance (X#stacja.wspolrzedne, Wspolrzedne) < Promien],
  PomiaryZeStacji = [getStationSum(X, TypPomiaru) || X<-PrzefiltrowaneStacje],  %srednie ze stacji maja postac: {LiczbaPomiarow danego typu, Suma wartosci tych pomiarow}
  LiczbaPomiarow = lists:foldl(fun (X,Y) -> element(1,X) + Y end, 0, PomiaryZeStacji),
  SumaPomiarow = lists:foldl(fun (X,Y) -> element(2,X) + Y end, 0, PomiaryZeStacji),
  case LiczbaPomiarow of
    0 -> {error, noMeasurements};
    _ -> SumaPomiarow / LiczbaPomiarow
  end;


getAreaMean (Nazwa, TypPomiaru, Promien, Monitor) when is_list(Nazwa) ->
  Stacja = lists:keyfind(Nazwa, 2, Monitor#monitor.stacje),
  getAreaMean(Stacja#stacja.wspolrzedne, TypPomiaru, Promien, Monitor).

distance (Wspolrzedne1, Wspolrzedne2) ->
  X = element (1, Wspolrzedne1),
  Y = element (2, Wspolrzedne1),
  A = element (1, Wspolrzedne2),
  B = element (2, Wspolrzedne2),
  math:sqrt((X-A) * (X-A) + (Y-B) * (Y-B)).

getStationSum ({}, _TypPomiaru) ->
  {0,0};

getStationSum (Stacja, TypPomiaru) ->
  PrzefiltrowanePomiary = [X#pomiar.wartosc || X<-Stacja#stacja.pomiary, rodzajPomiaru(X) == TypPomiaru],
  LiczbaPomiarow = lists:flatlength(PrzefiltrowanePomiary),
  Suma = lists:foldl(fun (X, Y) -> X + Y end, 0, PrzefiltrowanePomiary),    %fun (X, Y) -> X + Y end to lambda. Mozna by tez uzyc samodzielnie zdefiniowanej funkcji suma (X, Y) -> X + Y.
  {LiczbaPomiarow, Suma}.


%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

testPoprawny1() ->
  P = createMonitor(),
  P1 = addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = addValue("Aleja Slowackiego", calendar:local_time(), 'PM10', 59, P2),
  P4 = pollution:addValue({50.2345, 18.3445}, calendar:local_time(), 'PM10', 59, P3),
  P5 = pollution:removeValue ("Aleja Slowackiego", {{2017,4,23},{19,16,1}}, 'PM10', P4),
  pollution:removeValue ({50.2345,18.3445}, {{2017,4,23},{19,16,1}}, 'PM2.5', P5).
%ma zwrocic monitor z dwoma stacjami, gdzie druga ma dwa pomiary

testPoprawny2 () ->
  P = createMonitor(),
  P1 = addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = addValue("Aleja Slowackiego", {{2017,4,23},{19,16,1}}, 'PM10', 59, P2),
  P4 = addValue({50.2345, 18.3445}, calendar:local_time(), 'PM10', 100, P3),
  removeValue ("Aleja Slowackiego", {{2017,4,23},{19,16,1}}, 'PM10', P4).
%ma zwrocic monitor z dwoma stacjami, gdzie druga ma dokładnie jeden pomiar

testPoprawny3 () ->
  P = createMonitor(),
  P1 = addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = addValue("Aleja Slowackiego", {{2017,4,23},{19,16,1}}, 'PM10', 59, P2),
  P4 = addValue({50.2345, 18.3445}, calendar:local_time(), 'Temperatura', 10, P3),
  {getOneValue("Aleja Slowackiego", {{2017,4,23},{19,16,1}}, 'PM10', P4), getOneValue({50.2345, 18.3445}, calendar:local_time(), 'Temperatura', P4), getOneValue("Aleja Mickiewicza", {{2017,4,23},{19,16,1}}, 'PM10', P4)}.
%powinien zwrócić krotkę: {59, 10, -1}, czyli wartosci dwoch pomiarow i -1 dla nieistniejacego pomiaru

testPoprawny4 () ->
  P = createMonitor(),
  P1 = addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = addValue("Aleja Slowackiego", {{2017,4,23},{19,16,1}}, 'PM10', 59, P2),
  P4 = addValue({50.2345, 18.3445}, calendar:local_time(), 'PM10', 10, P3),
  getStationMean({50.2345, 18.3445}, 'PM10', P4).
%srednia: 69/2 = 34,5

testPoprawny5 () ->
  P = createMonitor(),
  P1 = addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
  P4 = addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80, P3),
  P5 = addValue("Aleja Mickiewicza", {{2017,4,23},{19,0,1}}, 'PM10', 40, P4),
  getDailyMean('PM10', {2017,4,23}, P5).
%powinien zwrócić 60


testPoprawny6 () ->
  P = createMonitor(),
  P1 = addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
  P4 = addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80, P3),
  P5 = addValue("Aleja Mickiewicza", {{2017,4,23},{19,0,1}}, 'PM10', 40, P4),
  {getAreaMean("Aleja Mickiewicza", 'PM10', 1, P5), getAreaMean("Aleja Mickiewicza", 'PM10', 12, P5)}.
%krotka, w pierwszym polu średnia z Aleji Mickiewicza (40), w drugim średnia z obu (60)


%  c("src/pollution.erl").
%  pollution:testPoprawnyX(). gdzie X to numer testu


getStationDailySumTest () ->
  getStationDailySum({}, 'PM10', {2017,4,23}).


%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. kwi 2017 12:42
%%%-------------------------------------------------------------------
-module(pollution_test).
-author("Karol").

-include_lib("eunit/include/eunit.hrl").
%-include("pollution.erl").

%simple_test() ->
  %c ("C:\Users\Karol\IdeaProjects\Elab2\src\pollution.erl"),


getOneValue_test () ->
  P = pollution:createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = pollution:addValue("Aleja Slowackiego", {{2017,4,23},{19,16,1}}, 'PM10', 59, P2),
  P4 = pollution:addValue({50.2345, 18.3445}, calendar:local_time(), 'Temperatura', 10, P3),
  ?assertEqual ({59, 10, -1},{pollution:getOneValue("Aleja Slowackiego", {{2017,4,23},{19,16,1}}, 'PM10', P4), pollution:getOneValue({50.2345, 18.3445}, calendar:local_time(), 'Temperatura', P4), pollution:getOneValue("Aleja Mickiewicza", {{2017,4,23},{19,16,1}}, 'PM10', P4)}).
%powinien zwrócić krotkę: {59, 10, -1}, czyli wartosci dwoch pomiarow i -1 dla nieistniejacego pomiaru

%TESTY WYLOSOWANEJ FUNKCJI getAreaMean
%  getAreaMean (Nazwa, TypPomiaru, Promien, Monitor)
%  getAreaMean (Wspolrzedne, TypPomiaru, Promien, Monitor)

getAreaMean_1_test () ->
  P = pollution:createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  ?assertEqual (-1.0, pollution:getAreaMean("Aleja Slowackiego", 'PM10', 10, P1)).

getAreaMean_2_test () ->
  P = pollution:createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  ?assertEqual (-1.0, pollution:getAreaMean({50.2345, 18.3445}, 'PM10', 10, P1)).

getAreaMean_3_test () ->
  P = pollution:createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = pollution:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
  ?assertEqual (60.0, pollution:getAreaMean("Aleja Slowackiego", 'PM10', 100, P3)).

getAreaMean_4_test () ->
  P = pollution:createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = pollution:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
  ?assertEqual (-1.0, pollution:getAreaMean({1.1, 1.1}, 'PM10', 10, P3)).

getAreaMean_5_test () ->
  P = pollution:createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = pollution:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
  P4 = pollution:addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80, P3),
  P5 = pollution:addValue("Aleja Mickiewicza", {{2017,4,23},{19,0,1}}, 'PM10', 40, P4),
  ?assertEqual ({40.0, 60.0}, {pollution:getAreaMean("Aleja Mickiewicza", 'PM10', 1, P5), pollution:getAreaMean("Aleja Mickiewicza", 'PM10', 12, P5)}).



%  P = createMonitor(),
%  P1 = addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
%  P2 = addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
%  P3 = addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
%  P4 = addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80, P3),
%  P5 = addValue("Aleja Mickiewicza", {{2017,4,23},{19,0,1}}, 'PM10', 40, P4),
%  {getAreaMean("Aleja Mickiewicza", 'PM10', 1, P5), getAreaMean("Aleja Mickiewicza", 'PM10', 12, P5)}.
%krotka, w pierwszym polu średnia z Aleji Mickiewicza (40), w drugim średnia z obu (60)




%TESTY DO DOCA:

%Siódma funkcja, getDailyMean:
%założenia: można podawać współrzędne albo odwoływać się do stacji poprzez nazwę
%pomiary mogą miec wartość zero, dlatego dla odróżnienia zakładam, że przy braku pomiarów funkcja zwraca -1.0


getDailyMean_1_test () ->
  P =pollution: createMonitor(),
  ?assertEqual (-1.0 , pollution:getDailyMean('PM10', {2017,4,23}, P)).

getDailyMean_2_test () ->
  P =pollution: createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  ?assertEqual (-1.0 , pollution:getDailyMean('PM10', {2017,4,23}, P2)).

getDailyMean_3_test () ->
  P =pollution: createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = pollution:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
  ?assertEqual (60.0 , pollution:getDailyMean('PM10', {2017,4,23}, P3)).

getDailyMean_4_test () ->
  P =pollution: createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = pollution:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
  P4 = pollution:addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80, P3),
  ?assertEqual (70.0 , pollution:getDailyMean('PM10', {2017,4,23}, P4)).

getDailyMean_5_test () ->
  P =pollution: createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = pollution:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
  P4 = pollution:addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80, P3),
  P5 = pollution:addValue("Aleja Mickiewicza", {{2017,4,23},{19,0,1}}, 'PM10', 40, P4),
  ?assertEqual (60.0 , pollution:getDailyMean('PM10', {2017,4,23}, P5)).

getDailyMean_6_test () ->
  P =pollution: createMonitor(),
  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
  P3 = pollution:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
  P4 = pollution:addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80, P3),
  P5 = pollution:addValue("Aleja Mickiewicza", {{2017,4,23},{19,0,1}}, 'PM2.5', 40, P4),
  ?assertEqual (70.0 , pollution:getDailyMean('PM10', {2017,4,23}, P5)).





%  P =pollution: createMonitor(),
%  P1 = pollution:addStation("Aleja Slowackiego", {50.2345, 18.3445}, P),
%  P2 = pollution:addStation("Aleja Mickiewicza", {58.2345, 10.3445}, P1),
%  P3 = pollution:addValue("Aleja Slowackiego", {{2017,4,23},{19,0,1}}, 'PM10', 60, P2),
%  P4 = pollution:addValue({50.2345, 18.3445}, {{2017,4,23},{21,0,1}}, 'PM10', 80, P3),
%  P5 = pollution:addValue("Aleja Mickiewicza", {{2017,4,23},{19,0,1}}, 'PM10', 40, P4),
%  pollution:getDailyMean('PM10', {2017,4,23}, P5).

%  ?assertEqual (70 , pollution:getDailyMean('PM10', {2017,4,23}, P)),

%mam napisać testy do:
%  getDailyMean/3 - zwraca średnią wartość parametru danego typu, danego dnia na wszystkich stacjach;

%  http://www.goo.gl/kdBFaO
% tam wrzucać testy

%WAŻNE!
%spróbować zrobić serwer pollution, tyle że bez tworzenia nowego monitora, a każda funkcja ma o jeden argument mniej- bez podawania monitora


%  c("src/pollution_test.erl").
%  pollution_test:test().
% to powinno odpalić wszystkie testy i generatory testów





%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 04. kwi 2017 11:36
%%%-------------------------------------------------------------------
-module(qsort).
-author("Karol").

%% API
-export([lessThan/2, grtEqThan/2, qs/1, randomElems/3, compareSpeeds/3, compareSpeeds2/3, compareSpeeds3/3]).


lessThan (List, Arg) -> [X || X <- List, X < Arg].


grtEqThan(List, Arg) -> [X || X <- List, X >= Arg].

qs([Pivot|Tail]) -> qs( lessThan(Tail,Pivot) ) ++ [Pivot] ++ qs( grtEqThan(Tail,Pivot) );
qs([]) -> [].

randomElems(N,Min,Max)-> [Min + rand:uniform(Max - Min) || _ <- lists:seq(1, N)].

compareSpeeds(List, Fun1, Fun2) ->  {  {T1 = {T1, _} = timer:tc (Fun1, [List])}, {T2 = {T2,_} = timer:tc (Fun2, [List])}}.

compareSpeeds2(List, Fun1, Fun2) ->  {  timer:tc (Fun1, [List]), timer:tc (Fun2, [List])}.

compareSpeeds3(List, Fun1, Fun2) ->  {  element(1,timer:tc (Fun1, [List])), element(1,timer:tc (Fun2, [List]))}.

%% c("src/qsort.erl").
%% List5 = qsort:randomElems(20000,1,5000).
%% QS = fun qsort:qs/1.
%% LS = fun lists:sort/1.
%% qsort:compareSpeeds3(List5,QS,LS).

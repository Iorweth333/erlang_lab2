%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. kwi 2017 19:29
%%%-------------------------------------------------------------------
-module('fun').
-author("Karol").

%% API
-export([map/2,filter/2, isEven/1]).


% Zaimplementuj samodzielnie funkcje wyższego rzędu map/2 oraz filter/2.
% Stwórz funkcję, która policzy sumę cyfr w liczbie. Użyj do tego lists:foldl/3.
% Przy pomocy funkcji lists:filter/2 wybierz z listy miliona losowych liczb takie,
% w których suma cyfr jest podzielna przez 3

map (Fun, List) -> [Fun(X) || X <- List].

filter (Fun, List) -> [X || X <- List, Fun(X)].

isEven (X) when X rem 2 ==0  -> true;
isEven (_) -> false.


% c("src/fun.erl").
% c("src/qsort.erl").
% List1 = qsort:randomElems(10, 1, 10).
% 'fun':map(fun (X) -> X*X end, List1).
% IsEven = fun 'fun':isEven/1.
% 'fun':filter(IsEven, List1).